#include <stdio.h>
#include <string.h>

#define NELEMS(X) (sizeof(X)/sizeof((X)[0]))

#define NROWS 3
#define NCOLS 20
#define NPENT NELEMS(pentominoes)

#include "pentominoes.c"

static char grid[NROWS][NCOLS];

static int
pentomino_fits(int row, int col, int pent, int ori)
{
    for (int r = 0; r < 5; r++) {
        for (int c = 0; c < 5; c++) {
            if (pentominoes[pent][ori][r][c] != ' ') {
                if (row+r >= NROWS || col+c >= NCOLS)
                    return 0;
                if (grid[row+r][col+c])
                    return 0;
            }
        }
    }
    return 1;
}

static void
place(int row, int col, int pent, int ori)
{
    for (int r = 0; r < 5; r++) {
        for (int c = 0; c < 5; c++) {
            char ch = pentominoes[pent][ori][r][c];
            if (ch != ' ') {
                grid[row+r][col+c] = ch;
            }
        }
    }
}

static void
unplace(int row, int col, int pent, int ori)
{
    for (int r = 0; r < 5; r++) {
        for (int c = 0; c < 5; c++) {
            if (pentominoes[pent][ori][r][c] != ' ')
                grid[row+r][col+c] = '\0';
        }
    }
}

static void
print_grid()
{
    for (int r = 0; r < NROWS; r++) {
        for (int c = 0; c < NCOLS; c++) {
            char ch = grid[r][c];
            if (ch == '\0')
                printf(".");
            else
                printf("%c", ch);
        }
        printf("\n");
    }
    printf("\n");
}

static int checked[NROWS][NCOLS];

static int
spanning(int r, int c)
{
    if (checked[r][c])
        return 0;
    checked[r][c] = 1;
    if (grid[r][c])
        return 0;
    int result = 1;
    if (r > 0)       result += spanning(r - 1, c);
    if (r < NROWS-1) result += spanning(r + 1, c);
    if (c > 0)       result += spanning(r, c - 1);
    if (c < NCOLS-1) result += spanning(r, c + 1);
    return result;
}

static int
solve(int p)
{
    memset(checked, 0, sizeof(checked));
    for (int r = 0; r < NROWS; r++) {
        for (int c = 0; c < NCOLS; c++) {
            int s = spanning(r, c);
            if (s > 0 && s % 5 != 0)
                return 0;
        }
    }
    for (int r = 0; r < NROWS; r++) {
        for (int c = 0; c < NCOLS; c++) {
            for (int o = 0; o < NELEMS(pentominoes[p]); o++) {
                if (pentomino_fits(r, c, p, o)) {
                    place(r, c, p, o);
                    //if (p < 3) {
                    //    printf("\x1b[H\x1b[2J");
                    //    print_grid();
                    //}
                    if (p == NPENT-1)
                        return 1;
                    if (solve(p+1))
                        return 1;
                    unplace(r, c, p, o);
                }
            }
        }
    }
    return 0;
}

int main()
{
    printf("%d\n", solve(0));
    print_grid();
}
